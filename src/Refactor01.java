/**
 * Ejemplo de c�digo Java Repetido
 * 
 * @author      Paco Gomez <fjgomez@florida-uni.es>
 * @version     1.0 
 * @since       1.2          (the version of the package this class was first added to)
 */
public class Refactor01 {

	public static void main(String[] args) {
		 	float area;
		 	//Variables
		 	int lado=3,radio=3,base=3,altura=6; 
	        float pi=3.14f;
	        //Calculador de areas
	        area=lado*lado;
	        area=base*altura;
	        area=pi*radio*radio;

	        //Cuadrado
	        String titulo = "CUADRADO";
	        muestra(titulo, area);
	        //Circulo
	        titulo = "CIRCULO";
	        muestra(titulo, area);
	        //Rectangulo
	        titulo = "RECTANGULO";
	        muestra(titulo, area);
	    }
	    public static void muestra(String titulo, float valor)
	    {
	        System.out.println("---- AREA DE UN "+titulo+" ------");
	        System.out.println(valor);
	        System.out.println("-------------------------------");
	    }
	}
